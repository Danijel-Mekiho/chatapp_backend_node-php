-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: Jun 25, 2017 at 05:56 AM
-- Server version: 5.5.42
-- PHP Version: 7.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `whatsCloneF`
--


  ALTER TABLE `wa_groups`
    CHANGE `name` `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL;

  ALTER TABLE `wa_status`
        CHANGE `status` `status` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL;

          INSERT INTO `wa_settings` (`id`, `name`, `value`) VALUES
          (20, 'sms_msg91_verification', '0'),
          (21, 'sms_msg_91_authentication_key', 'put your SMS Msg91 provider authentication key here');
