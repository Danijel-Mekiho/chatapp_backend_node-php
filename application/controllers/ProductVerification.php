<?php

class ProductVerification  {

	private $itemId = '17553421'; // Item ID
	private $username = 'BenCherif'; // Envato Username
	private $apiKey = 'dehow7jxa4sp02urhisbjlzfimjbgg4u'; // Envato API
	private $error = NULL;


    public function showDetail($purchaseCode){

        // From envato
        $url = "https://marketplace.envato.com/api/edge/".$this->username."/".$this->apiKey."/verify-purchase:".$purchaseCode.".json";

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER,  array('User-Agent: My-Great-Marketplace-App'));
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $data = curl_exec($curl);
        $data = json_decode($data,true);

        echo "<pre>";
        print_r($data);
        echo "</pre>";

    }

    public function checkPurchaseID($id){

    }

	 /**
     * Gets the value of id.
     * @return mixed
     */
    public function getError()
    {
        return $this->error;
    }

    public function getItemId()
    {
        return $this->itemId;
    }

    /**
     * Sets the value of id.
     * @param mixed $id the id
     * @return self
     */
    public function setItemId($itemId)
    {
        $this->itemId = $itemId;
        return $this;
    }


	/**
	 * __construct function.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {

	}

	public function check($purchaseCode,$url){


		// Check Validate the purchase code
		if(!$this->validPCFormat($purchaseCode)){
			return false;
		}

		// check if the purchase code is valid
		if(!$this->EnvatoPCCheck($purchaseCode,$url)){
			return false;
		}


		// You can check the purchase code on your local database here

		return true;
	}

	public function EnvatoPCCheck($purchaseCode,$url){

		// From envato
		$url = "https://marketplace.envato.com/api/edge/".$this->username."/".$this->apiKey."/verify-purchase:".$purchaseCode.".json";

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER,  array('User-Agent: My-Great-Marketplace-App'));
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $data = curl_exec($curl);
        $data = json_decode($data,true);

        //close
        curl_close($curl);

        if($data){
			if (array_key_exists('verify-purchase', $data) && array_key_exists('item_id', $data["verify-purchase"])) {
				if($data["verify-purchase"]["item_id"] == $this->itemId){
					// The purchase code is valid do whatever your want here
					return true;
				}
			}
		}

		return false;
	}

	public function validPCFormat($purchaseCode){
		return preg_match('/([a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12})/', $purchaseCode);
	}



    function checkIsDupplicated($pid,$uri){
        return FALSE;

    }

}
